#include <iostream>


int fib(int n)
{
    int a = 0;
    int b = 1;
    int c;
    int i;

    if (n == 0)
        return a;

    for (i = 2; i <= n; i++)
    {
        c = a + b;
        a = b;
        b = c;
    }

    return b;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
        return -1;

    int n = atoi(argv[1]);

    if (n <= 0)
        return -1;

    std::cout << fib(n) << std::endl;

    return 0;
}
