FROM ubuntu:latest

COPY ./build/bin/*.deb /tmp/

RUN apt update && apt install -y /tmp/*.deb && rm -rf /tmp/*.deb
