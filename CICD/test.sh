cd build

input="10"
expected_output="55"

output=$(./Fibonacci "$input")

if [ "$output" == "$expected_output" ]; then
    echo "Test passed"
else
    echo "Test failed"
    exit 1
fi
